This module provide integration http://platon.ua payments for
https://www.drupal.org/project/payment (7.x-1.x version)

Registering with Platon
Before you start the installation process you must register on Platon.ua
and create your own Merchant. You will get "Merchant key", "Password" and
other settings for your payment system.

Installation and Configuration
Download the module from Drupal.org and extract it to your modules folder.
Enable it.
Go to /admin/config/services/payment/method/add and add Platon payment method.
Setup the settings according your data from Platon.

Additional information:

Default Process URL: http://yoursitename.com/platon/process
Default Success URL: http://yoursitename.com/platon/success
Default Error URL: http://yoursitename.com/platon/error

More API documentation can be found at Interkassa API
