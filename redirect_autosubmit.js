/**
 * @file
 * Automatically submit the payment redirect form.
 */

(function ($) {
  Drupal.behaviors.platonPayment = {
    attach: function (context, settings) {
      $('div.platon-redirect-form form', context).submit();
    }
  };
})(jQuery);
