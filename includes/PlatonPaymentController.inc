<?php
/**
 * @file
 * Contains PlatonPaymentController.
 */

/**
 * A Platon payment method controller.
 */
class PlatonPaymentController extends PaymentMethodController {

  /**
   * Platon server payment URL.
   */
  const SERVER_URL = 'https://secure.platononline.com/payment/auth';

  /**
   * Default controller data.
   *
   * @var array
   */
  public $controller_data_defaults = array(
    'merchant' => '',
    'password' => '',
    'url' => '',
    'error_url' => '',
  );

  /**
   * Payment method configuration form elements callback.
   *
   * @var string
   */
  public $payment_method_configuration_form_elements_callback = 'platon_payment_payment_method_configuration_form_elements';

  /**
   * Payment configuration form elements callback.
   *
   * @var string
   */
  public $payment_configuration_form_elements_callback = 'platon_payment_payment_configuration_form_elements';

  /**
   * PlatonPaymentController constructor.
   */
  public function __construct() {
    $this->title = t('Platon');
    $this->description = t('Platon payment method.');
  }

  /**
   * Implements PaymentMethodController::execute().
   */
  public function execute(Payment $payment) {
    entity_save('payment', $payment);
    drupal_goto('platon/redirect/' . $payment->pid);
  }

  /**
   * Payment form generator.
   *
   * @param \Payment $payment
   *   Payment object.
   *
   * @return array
   *   Payment form array.
   */
  static public function getPaymentForm(Payment $payment) {
    $form = array();
    $form_data = array(
      'key' => $payment->method->controller_data['merchant'],
      'order' => $payment->pid,
      // CC for card.
      'payment' => 'CC',
      'data' => self::encodeData($payment),
      'url' => $payment->method->controller_data['url'],
      'error_url' => $payment->method->controller_data['error_url'],
      'sign' => self::signature($payment),
    );
    $form['#action'] = self::SERVER_URL;
    foreach ($form_data as $key => $value) {
      $form[$key] = array(
        '#type' => 'hidden',
        '#value' => $value,
      );
    }
    return $form;
  }

  /**
   * Post data validator.
   *
   * @param array $data
   *   Post data.
   *
   * @return bool
   *   TRUE if post data is valid FALSE otherwise.
   */
  static public function validatePost(array $data) {
    if (empty($data) || !isset($data['order'])) {
      return FALSE;
    }

    $payment = entity_load_single('payment', $data['order']);
    if (!$payment) {
      return FALSE;
    }

    $sing_data = array(
      strrev($data['email']),
      $payment->method->controller_data['password'],
      $data['order'],
      strrev(drupal_substr($data['card'], 0, 6) . drupal_substr($data['card'], -4)),
    );
    $upcase_sign_data = array_map('drupal_strtoupper', $sing_data);
    $signature_string = implode('', $upcase_sign_data);
    $sign = md5($signature_string);
    if ($data['sign'] != $sign) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Calculate currency rate based on privatbank.ua information.
   *
   * Because legal requirements it is need to make any payments in UAH.
   * There is no National bank of Ukraine official currency rate API so we use
   * Privatbank.ua API as biggest and most modern bank in Ukraine.
   *
   * @param string $currency
   *   Currency code name.
   *
   * @return int
   *   Currency to UAH rate value.
   */
  static public function currencyRate($currency = 'UAH') {
    $result = 1;
    $url = 'https://api.privatbank.ua/p24api/pubinfo';
    $data = array(
      'json' => 1,
      'exchange' => 1,
      // Request privatbank.ua currency exchange rates.
      'coursid' => 5,
    );
    $full_url = url($url, array('query' => $data));;
    $response = drupal_http_request($full_url);
    if ($response && $response->code == '200') {
      $rates = drupal_json_decode($response->data);
      foreach ($rates as $rate) {
        if ($rate['ccy'] == $currency) {
          $result = $rate['buy'];
        }
      }
    }
    return $result;
  }

  /**
   * Payment data encode method.
   *
   * @param \Payment $payment
   *   Payment object.
   *
   * @return string
   *   base64 encoded payment data.
   */
  static public function encodeData(Payment $payment) {
    $data = array(
      'amount' => number_format($payment->totalAmount(TRUE) * self::currencyRate($payment->currency_code), 2, '.', ''),
      'currency' => 'UAH',
      'description' => format_string($payment->description, $payment->description_arguments),
    );
    $json = drupal_json_encode($data);
    return base64_encode($json);
  }

  /**
   * Generate payment signature.
   *
   * @param \Payment $payment
   *   Payment object.
   *
   * @return string
   *   Signature.
   */
  static public function signature(Payment $payment) {
    $signature_data = array(
      $payment->method->controller_data['merchant'],
      'CC',
      self::encodeData($payment),
      $payment->method->controller_data['url'],
      $payment->method->controller_data['password'],
    );
    $signature_data = array_map('strrev', $signature_data);
    $signature_data = array_map('drupal_strtoupper', $signature_data);
    $signature_string = implode('', $signature_data);
    return md5($signature_string);
  }

}
